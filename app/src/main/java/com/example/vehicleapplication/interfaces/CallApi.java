package com.example.vehicleapplication.interfaces;

import com.example.vehicleapplication.model.VehiclesResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Bageshri on 4/27/2017.
 */

public interface CallApi {

    @GET("/vehicles")
    Call<VehiclesResponse> getVehicleList();
}
