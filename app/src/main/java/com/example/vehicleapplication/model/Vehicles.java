package com.example.vehicleapplication.model;

/**
 * Created by Bageshri on 4/27/2017.
 */

public class Vehicles {

    private String vehicleId;
    private String vrn;
    private String country;
    private String color;
    private String type;

    public Vehicles(String vehicleId, String vrn, String country, String color, String type) {
        this.vehicleId = vehicleId;
        this.vrn = vrn;
        this.country = country;
        this.color = color;
        this.type = type;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVrn() {
        return vrn;
    }

    public void setVrn(String vrn) {
        this.vrn = vrn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
