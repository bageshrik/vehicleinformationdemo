package com.example.vehicleapplication.model;

import java.util.ArrayList;

/**
 * Created by Bageshri on 4/27/2017.
 */

public class VehiclesResponse {

    private String count;
    private ArrayList<Vehicles> vehicles;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ArrayList<Vehicles> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<Vehicles> vehicles) {
        this.vehicles = vehicles;
    }
}
