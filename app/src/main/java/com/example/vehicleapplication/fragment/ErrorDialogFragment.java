package com.example.vehicleapplication.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vehicleapplication.R;

/**
 * Created by Bageshri on 4/28/2017.
 */

public class ErrorDialogFragment extends DialogFragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_error, container, false);
        initComponent(view);
        getDialog().setTitle("Network Problem");
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return super.onCreateDialog(savedInstanceState);
    }

    private void initComponent(View view) {
        view.findViewById(R.id.setting).setOnClickListener(this);
        view.findViewById(R.id.ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting:
                startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                dismiss();
                break;
            case R.id.ok:
                dismiss();
                break;
        }
    }
}
