package com.example.vehicleapplication.util;

/**
 * Created by Bageshri on 4/28/2017.
 */

public class Constants {

    public static final String VEHICLE_ID = "vehicle_id";
    public static final String VRN = "vrn";
    public static final String COUNTRY = "country";
    public static final String COLOR = "color";
    public static final String TYPE = "type";

    public static final String VEHICLE_INFORMATION = "Vehicle Information";
}
