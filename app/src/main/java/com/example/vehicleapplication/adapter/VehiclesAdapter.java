package com.example.vehicleapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vehicleapplication.R;
import com.example.vehicleapplication.activity.VehicleDetailsActivity;
import com.example.vehicleapplication.model.Vehicles;
import com.example.vehicleapplication.util.Constants;

import java.util.ArrayList;

/**
 * Created by Bageshri on 4/27/2017.
 */

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.VehicleViewHolder> {

    private ArrayList<Vehicles> vehiclesArrayList;
    private Context context;
    private int rowLayout;

    public static class VehicleViewHolder extends RecyclerView.ViewHolder {
        CardView cardViewVehicles;
        TextView textViewVehicleName;


        public VehicleViewHolder(View v) {
            super(v);
            cardViewVehicles = (CardView) v.findViewById(R.id.cardViewVehicles);
            textViewVehicleName = (TextView) v.findViewById(R.id.textViewVehicleName);
        }
    }

    public VehiclesAdapter(ArrayList<Vehicles> vehiclesArrayList, int rowLayout, Context context) {
        this.vehiclesArrayList = vehiclesArrayList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public VehiclesAdapter.VehicleViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new VehicleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VehicleViewHolder holder, final int position) {
        holder.textViewVehicleName.setText(vehiclesArrayList.get(position).getVrn());

        holder.cardViewVehicles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VehicleDetailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.VEHICLE_ID, vehiclesArrayList.get(position).getVehicleId());
                intent.putExtra(Constants.VRN, vehiclesArrayList.get(position).getVrn());
                intent.putExtra(Constants.COUNTRY, vehiclesArrayList.get(position).getCountry());
                intent.putExtra(Constants.COLOR, vehiclesArrayList.get(position).getColor());
                intent.putExtra(Constants.TYPE, vehiclesArrayList.get(position).getType());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehiclesArrayList.size();
    }
}
