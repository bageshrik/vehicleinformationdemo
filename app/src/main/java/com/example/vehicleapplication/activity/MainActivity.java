package com.example.vehicleapplication.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.vehicleapplication.R;
import com.example.vehicleapplication.adapter.VehiclesAdapter;
import com.example.vehicleapplication.fragment.ErrorDialogFragment;
import com.example.vehicleapplication.interfaces.CallApi;
import com.example.vehicleapplication.model.Vehicles;
import com.example.vehicleapplication.model.VehiclesResponse;
import com.example.vehicleapplication.util.NetworkUtils;
import com.example.vehicleapplication.util.RetrofitClient;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView recyclerViewVehiclesList;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkNetwork();

        recyclerViewVehiclesList = (RecyclerView) findViewById(R.id.recyclerViewVehiclesList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewVehiclesList.setLayoutManager(layoutManager);

        getVehicleList();
    }

    /**
     * @desc Unit test cases
     * 1.Test case - To get vehicle list
     * Procedure - Get data from server
     * Expected results - Display vehicle list
     */
    private void getVehicleList() {

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading vehicles...");
        mProgressDialog.show();

        CallApi callApi =
                RetrofitClient.getClient().create(CallApi.class);

        Call<VehiclesResponse> call = callApi.getVehicleList();
        call.enqueue(new Callback<VehiclesResponse>() {
            @Override
            public void onResponse(Call<VehiclesResponse> call, Response<VehiclesResponse> response) {
                ArrayList<Vehicles> vehiclesArray = response.body().getVehicles();

                Log.d(TAG, "Number of vehicles received: " + vehiclesArray.size());
                recyclerViewVehiclesList.setAdapter(new VehiclesAdapter(vehiclesArray, R.layout.card_vehicle, getApplicationContext()));
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<VehiclesResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
            }
        });
    }

    /**
     * @desc Unit test cases
     * 1.Test case - To check network connection
     * Procedure - Check for Connection manager
     * Expected results - Connection connected success or failure
     */
    private void checkNetwork() {

        if (!NetworkUtils.isNetworkAvailable(this)) {
            ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
            errorDialogFragment.show(getSupportFragmentManager(), "network_error_dialog");
        }
    }
}


