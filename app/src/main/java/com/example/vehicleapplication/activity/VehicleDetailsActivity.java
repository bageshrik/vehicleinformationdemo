package com.example.vehicleapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.vehicleapplication.R;
import com.example.vehicleapplication.util.Constants;

public class VehicleDetailsActivity extends AppCompatActivity {

    private TextView textViewVehicleId, textViewVehicleVrn, textViewVehicleCountry, textViewVehicleColor, textViewVehicleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_details);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Intent intent = getIntent();
        String vehicle_id = intent.getStringExtra(Constants.VEHICLE_ID);
        String vrn = intent.getStringExtra(Constants.VRN);
        String country = intent.getStringExtra(Constants.COUNTRY);
        String color = intent.getStringExtra(Constants.COLOR);
        String type = intent.getStringExtra(Constants.TYPE);

        setTitle(Constants.VEHICLE_INFORMATION+ "- "+ vrn);

        textViewVehicleId = (TextView) findViewById(R.id.textViewVehicleId);
        textViewVehicleVrn = (TextView) findViewById(R.id.textViewVehicleVrn);
        textViewVehicleCountry = (TextView) findViewById(R.id.textViewVehicleCountry);
        textViewVehicleColor = (TextView) findViewById(R.id.textViewVehicleColor);
        textViewVehicleType = (TextView) findViewById(R.id.textViewVehicleType);

        getVehicleDetails(vehicle_id, vrn, country, color, type);
    }

    /**
     * @desc Unit test cases
     * 1.Test case - To get vehicle information
     * Procedure - Get vehicle information from server
     * Expected results - Display vehicle information
     * @param vehicle_id
     * @param vrn
     * @param country
     * @param color
     * @param type
     */

    private void getVehicleDetails(String vehicle_id, String vrn, String country, String color, String type) {

        //Set values in TextView
        textViewVehicleId.setText("Vehicle ID:" + vehicle_id);
        textViewVehicleVrn.setText("VRN: " + vrn);
        textViewVehicleCountry.setText("Country: " + country);
        textViewVehicleColor.setText("Color: " + color);
        textViewVehicleType.setText("Type: " + type);
    }
}

